package main.services;

import java.util.Objects;

public class MemoryInfo {
    private int memory;
    private int occupiedMemory;

    public MemoryInfo(int memory, int occupiedMemory) {
        this.memory = memory;
        this.occupiedMemory = occupiedMemory;
    }

    public int getMemory() {
        return memory;
    }

    public int getOccupiedMemory() {
        return occupiedMemory;
    }

    @Override
    public String toString() {
        return "MemoryInfo{" +
                "memory=" + memory +
                ", occupiedMemory=" + occupiedMemory +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MemoryInfo that = (MemoryInfo) o;
        return memory == that.memory && occupiedMemory == that.occupiedMemory;
    }

    @Override
    public int hashCode() {
        return Objects.hash(memory, occupiedMemory);
    }
}
