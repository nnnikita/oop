package main.services;

import java.util.ArrayList;

public class Filtration {

    public ArrayList<Device> filterToArchitecture(Device[] devices, String architecture){
        if(devices == null){
            return null;
        }
        String archInProcessor;
        ArrayList<Device> result = new ArrayList<>();
        for(int i = 0; i < devices.length; i++){
            if(devices[i] == null){
                continue;
            }
            archInProcessor = devices[i].getProcessor().getDetails().split(" ")[2];
            if(archInProcessor.equals(architecture + ",")){
                result.add(devices[i]);
            }
        }
        return result;
    }

    public ArrayList<Device> filterToParametersOfProcessor(Device[] devices, long frequency,  long cache, long bitCapacity){
        if(devices == null){
            return null;
        }
        ArrayList<Device> result = new ArrayList<>();
        for(int i = 0; i < devices.length; i++){
            if(devices[i] == null){
                continue;
            }
            if(devices[i].getProcessor().getFrequency() >= frequency &&
                    devices[i].getProcessor().getCache() >= cache &&
                    devices[i].getProcessor().getBitCapacity() >= bitCapacity){
                result.add(devices[i]);
            }
        }
        return result;
    }

    public ArrayList<Device> filterToMemoryVolume(Device[] devices, int volume){
        if(devices == null){
            return null;
        }
        ArrayList<Device> result = new ArrayList<>();
        for(int i = 0; i < devices.length; i++){
            if(devices[i] == null){
                continue;
            }
            if(devices[i].getMemory().getMemoryInfo().getMemory() >= volume){
                result.add(devices[i]);
            }
        }
        return result;
    }

    public ArrayList<Device> filterToOccupiedMemory(Device[] devices, int occupiedMemory){
        if(devices == null){
            return null;
        }
        ArrayList<Device> result = new ArrayList<>();
        for(int i = 0; i < devices.length; i++){
            if(devices[i] == null){
                continue;
            }
            if(devices[i].getMemory().getMemoryInfo().getOccupiedMemory() >= occupiedMemory){
                result.add(devices[i]);
            }
        }
        return result;
    }
}
