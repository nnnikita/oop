package main.services;

public class ProcessorX86 extends Processor{
    public static final String ARCHITECTURE = "X86";

    public ProcessorX86(long frequency, long cache, long bitCapacity) {
        super(frequency, cache, bitCapacity);
    }

    @Override
    public String dataProcess(String data) {
        if(data == null){
            return null;
        }
        return data.toLowerCase();
    }

    @Override
    public String dataProcess(long data) {
        return String.valueOf(data * 100);
    }

    @Override
    public String toString() {
        return "Processor{ architecture= " + ARCHITECTURE +
                ", frequency=" + getFrequency() +
                ", cache=" + getCache() +
                ", bitCapacity=" + getBitCapacity() +
                '}';
    }
}
