package main.services;

import java.util.ArrayList;

public class Device {
    private Processor processor;
    private Memory memory;

    public Device(Processor processor, Memory memory) {
        this.processor = processor;
        this.memory = memory;
    }

    public Processor getProcessor() {
        return processor;
    }

    public Memory getMemory() {
        return memory;
    }

    /* – сохранение в память всех элементов в массиве*/
    public void save (String[] data){
        for (int i = 0; i < data.length; i++) {
            if(!memory.save(data[i])){
                System.out.println("Недостаточно памяти");
                return;
            }
        }
    }

    /* – вычитка всех элементов из памяти, затем стирание данных*/
    public String[] readAll(){
        ArrayList<String> buffer = new ArrayList<>();
        String bufferValue;

        while(true){
            bufferValue = memory.removeLast();
            if(bufferValue != null){
                buffer.add(0, bufferValue);
            } else {
                break;
            }
        }
        String[] result = new String[buffer.size()];
        buffer.toArray(result);
        return result;
    }

    /* – преобразование всех данных, записанных в памяти*/
    public void dataProcessing(){
        String[] buffer = readAll();
        for(int i = 0; i < buffer.length; i++){
            buffer[i] = processor.dataProcess(buffer[i]);
        }
        save(buffer);
    }

    /*получение строки с информацией о системе (информация о процессоре, памяти)*/
    public String getSystemInfo(){
        return processor.getDetails() + "\n" + memory.getMemoryInfo().toString();
    }


}
