package test;

import main.services.Memory;
import main.services.MemoryInfo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class MemoryTest {
    private final int capacityOfMemory = 5;
    private final Memory cut = new Memory(capacityOfMemory);

//    ********************************************************
    @ParameterizedTest
    @ValueSource(ints = {3, 5, 6})
    void saveTest(int countOfSave){
        boolean actual = false;
        for(int i = 0; i < countOfSave; i++){
            actual = cut.save("A");
        }
        if(countOfSave > 5){
            actual = !actual;
        }
        assertTrue(actual);
    }

//    ********************************************************

    static Arguments[] getMemoryInfoTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new MemoryInfo(5,0), 0),
                Arguments.arguments(new MemoryInfo(5,40), 2),
                Arguments.arguments(new MemoryInfo(5,100), 5),
        };
    }

    @ParameterizedTest
    @MethodSource("getMemoryInfoTestArgs")
    void getMemoryInfoTest(MemoryInfo expected, int countOfSave){
        for(int i = 0; i < countOfSave; i++){
            cut.save("A");
        }

        MemoryInfo actual = cut.getMemoryInfo();

        assertEquals(expected, actual);
    }
//    ********************************************************
    static Arguments[] readLastTestArgs(){
        return new Arguments[]{
            Arguments.arguments(null, 0),
            Arguments.arguments("1", 1),
            Arguments.arguments("4", 4),
        };
}


    @ParameterizedTest
    @MethodSource("readLastTestArgs")
    void readLastTest(String expected, int countOfSave){
        String actual;
        for(int i = 1; i <= countOfSave; i++){
            cut.save(String.valueOf(i));
        }

        actual = cut.readLast();

        assertEquals(expected, actual);
    }

//    ********************************************************

    static Arguments[] removeLastTest1Args(){
        return new Arguments[]{
                Arguments.arguments(null, 0),
                Arguments.arguments("1", 1),
                Arguments.arguments("4", 4),
                Arguments.arguments("5", 5),
                Arguments.arguments("5", 7),
        };
    }


    @ParameterizedTest
    @MethodSource("removeLastTest1Args")
    void removeLastTest1(String expected, int countOfSave){
        String actual;
        for(int i = 1; i <= countOfSave; i++){
            cut.save(String.valueOf(i));
        }
        actual = cut.removeLast();

        assertEquals(expected, actual);
    }

    static Arguments[] removeLastTest2Args(){
        return new Arguments[]{
                Arguments.arguments(null, 0),
                Arguments.arguments(null, 1),
                Arguments.arguments("3", 4),
                Arguments.arguments("4", 5),
                Arguments.arguments("4", 7),
        };
    }

    @ParameterizedTest
    @MethodSource("removeLastTest2Args")
    void removeLastTest2(String expected, int countOfSave){
        String actual;
        for(int i = 1; i <= countOfSave; i++){
            cut.save(String.valueOf(i));
        }
        cut.removeLast();

        actual = cut.readLast();

        assertEquals(expected, actual);
    }
}