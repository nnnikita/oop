package test;


import main.services.ProcessorX86;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class ProcessorX86Test {
    static final ProcessorX86 cut = new ProcessorX86(2_400_000_000L, 1000L, 1_000_000_000L);

    static Arguments[] dataProcessTestArgs(){
        return new Arguments[]{
                Arguments.arguments("qwert", "QWERT"),
                Arguments.arguments("qwert", "qWerT"),
                Arguments.arguments("", ""),
                Arguments.arguments(" rt ", " Rt "),
                Arguments.arguments("  ", "  "),
                Arguments.arguments(" -1", " -1"),
                Arguments.arguments(null, null),
        };
    }

    @ParameterizedTest
    @MethodSource("dataProcessTestArgs")
    void dataProcessTest(String expected, String data) {
        String actual = cut.dataProcess(data);

        assertEquals(expected, actual);
    }
    //    ******************************************

    static Arguments[] dataProcessTestArgs2(){
        return new Arguments[]{
                Arguments.arguments("50000", 500),
                Arguments.arguments("0", 0),
                Arguments.arguments("-105000", -1050),
        };
    }

    @ParameterizedTest
    @MethodSource("dataProcessTestArgs2")
    void dataProcessTest(String expected, long data) {
        String actual = cut.dataProcess(data);

        assertEquals(expected, actual);
    }
}