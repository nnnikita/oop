package test;

import main.services.ProcessorArm;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class ProcessorArmTest {

    static final ProcessorArm cut = new ProcessorArm(2_400_000_000L, 1000L, 1_000_000_000L);

    static Arguments[] dataProcessTestArgs(){
        return new Arguments[]{
                Arguments.arguments("QWERT", "qwert"),
                Arguments.arguments("QWERT", "qWerT"),
                Arguments.arguments("", ""),
                Arguments.arguments(" RT ", " rt "),
                Arguments.arguments("  ", "  "),
                Arguments.arguments(" -1", " -1"),
                Arguments.arguments(null, null),
        };
    }

    @ParameterizedTest
    @MethodSource("dataProcessTestArgs")
    void dataProcessTest(String expected, String data) {
        String actual = cut.dataProcess(data);

        assertEquals(expected, actual);
    }
//    ******************************************
    static Arguments[] dataProcessTestArgs2(){
        return new Arguments[]{
            Arguments.arguments("5", 500),
            Arguments.arguments("0", 0),
            Arguments.arguments("-10", -1050),
        };
    }

    @ParameterizedTest
    @MethodSource("dataProcessTestArgs2")
    void dataProcessTest(String expected, long data) {
        String actual = cut.dataProcess(data);

        assertEquals(expected, actual);
    }
}