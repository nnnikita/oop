package test;

import main.services.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class FiltrationTest {

    private final Filtration cut = new Filtration();

    static Processor p1 = new ProcessorArm(2_000_000_000L, 1_000_000_000L, 1_500_000_000L);
    static Processor p2 = new ProcessorX86(3_000_000_000L, 1_000_000_000L, 2_000_000_000L);
    static Memory m1 = new Memory(5);
    static Memory m2 = new Memory(10);
    static Device d1 = new Device(p1, m1);
    static Device d2 = new Device(p1, m2);
    static Device d3 = new Device(p2, m1);
    static Device d4 = new Device(p2, m2);
    static Device[] devices = {d1, d2, d3, d4};



    static Arguments[] filterToArchitectureTestArgs(){
        return new Arguments[]{
                Arguments.arguments(devices, new ArrayList<Device>(Arrays.asList(d1, d2)), "ARM"),
                Arguments.arguments(devices, new ArrayList<Device>(Arrays.asList(d3, d4)), "X86"),
                Arguments.arguments(new Device[]{null, d1, null}, new ArrayList<Device>(), "X86"),
                Arguments.arguments(null, null, "X86"),
        };
    }

    @ParameterizedTest
    @MethodSource("filterToArchitectureTestArgs")
    void filterToArchitectureTest(Device[] array, ArrayList<Device> expected, String architecture){
        ArrayList<Device> actual = cut.filterToArchitecture(array,architecture);

        assertEquals(expected, actual);
    }

//    *********************************************************************

    static Arguments[] filterToParametersOfProcessorTestArgs(){
        return new Arguments[]{
                Arguments.arguments(devices, new ArrayList<Device>(Arrays.asList(d3, d4)), 2_500_000_000L, 500_000_000L, 1_500_000_000L),
                Arguments.arguments(devices, new ArrayList<Device>(), 5_000_000_000L, 2_000_000_000L, 5_000_000_000L),
                Arguments.arguments(new Device[]{null, d1, null, d3}, new ArrayList<Device>(Arrays.asList(d3)), 1_500_000_000L, 500_000_000L, 1_600_000_000L),
                Arguments.arguments(null, null, 2_500_000_000L, 500_000_000L, 1_500_000_000L),
        };
    }

    @ParameterizedTest
    @MethodSource("filterToParametersOfProcessorTestArgs")
    void filterToParametersOfProcessorTest(Device[] array, ArrayList<Device> expected,
                                           long frequency,  long cache, long bitCapacity){
        ArrayList<Device> actual = cut.filterToParametersOfProcessor(array, frequency, cache, bitCapacity);

        assertEquals(expected, actual);
    }

//    *********************************************************************

    static Arguments[] filterToMemoryVolumeTestArgs() {
        return new Arguments[]{
                Arguments.arguments(devices, new ArrayList<Device>(Arrays.asList(d2, d4)), 6),
                Arguments.arguments(devices, new ArrayList<Device>(Arrays.asList(d1, d2, d3, d4)), 4),
                Arguments.arguments(devices, new ArrayList<Device>(), 11),
                Arguments.arguments(new Device[]{null, d1, null}, new ArrayList<Device>(), 7),
                Arguments.arguments(null, null, 5),
        };
    }


    @ParameterizedTest
    @MethodSource("filterToMemoryVolumeTestArgs")
    void filterToMemoryVolumeTest(Device[] array, ArrayList<Device> expected, int volume){
        ArrayList<Device> actual = cut.filterToMemoryVolume(array, volume);

        assertEquals(expected, actual);
    }

//    *********************************************************************

    static Arguments[] filterToOccupiedMemoryTestArgs() {
        m1.save("1");//20%
        m2.save("1");//10%
        return new Arguments[]{
                Arguments.arguments(devices, new ArrayList<Device>(Arrays.asList(d1, d3)), 15),
                Arguments.arguments(devices, new ArrayList<Device>(Arrays.asList(d1, d2, d3, d4)), 5),
                Arguments.arguments(devices, new ArrayList<Device>(), 25),
                Arguments.arguments(new Device[]{null, d1, null}, new ArrayList<Device>(Arrays.asList(d1)), 16),
                Arguments.arguments(null, null, 95),
        };
    }


    @ParameterizedTest
    @MethodSource("filterToOccupiedMemoryTestArgs")
    void filterToOccupiedMemoryTest(Device[] array, ArrayList<Device> expected, int occupiedMemory){
        ArrayList<Device> actual = cut.filterToOccupiedMemory(array, occupiedMemory);

        assertEquals(expected, actual);
    }
}